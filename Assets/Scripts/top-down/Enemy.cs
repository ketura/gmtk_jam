﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace GMTK
{
	[RequireComponent(typeof(UnitColor))]
	[RequireComponent(typeof(Damageable))]
	public class Enemy : MonoBehaviour
	{
		public Transform target;

		public Damageable Health;
		public int PointValue;

		public UnitColor color;
		public MeshRenderer rndr;
		public Material black;
		public Material grey;
		public Material white;

		public float SpeedBoost;
		public float MaxSpeedBoost;

		public int touchDamage;

		void Start()
		{
			color = GetComponent<UnitColor>();
			Health = GetComponent<Damageable>();
			switch (color.Color)
			{
				case EntityColor.Black:
					rndr.material = black;
					break;
				case EntityColor.Grey:
					rndr.material = grey;
					break;
				case EntityColor.White:
					rndr.material = white;
					break;
			}

			target = GameObject.FindGameObjectWithTag("Player").transform;
		}

		private void OnTriggerEnter2D(Collider2D other)
		{
			if (other.gameObject.tag == "Bullet")
			{
				Debug.Log("bullet hit");
				var bullet = other.gameObject.GetComponent<Bullet>();
				if (bullet.Targets == TargetType.Enemy)
				{
					bullet.BulletImpact();

					TakeHit(bullet.Damage);
				}
			}
			else if (other.gameObject.tag == "Explosion")
			{
				Debug.Log("explosion hit");
				var explosion = other.gameObject.GetComponent<Explosion>();
				TakeHit(explosion.Damage);
			}

			else if (other.gameObject.tag == "DashCone")
			{
				Debug.Log("Dash hit");
				var dash = other.gameObject.GetComponent<DashCone>();
				TakeHit(dash.Damage);
			}

		}

		public void TakeHit(int damage)
		{
			Health.Damage(damage);
			if (!Health.IsAlive)
			{
				Kill();
			}
		}

		void Update()
		{

		}

		public void Kill()
		{
			ScoreManager.Instance.AddScore(PointValue);
			GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().EnemyDestroyed(SpeedBoost, MaxSpeedBoost);
			Destroy(gameObject);
		}
	}
}