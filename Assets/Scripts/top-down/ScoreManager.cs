﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using TMPro;

namespace GMTK
{
	public class ScoreManager : Singleton<ScoreManager>
	{
		public string format = "{0}";

		public int Score = 0;

		public TMP_Text Scoreboard;

		private void Start()
		{
			if (!Scoreboard)
				Scoreboard = GetComponent<TMP_Text>();
		}

		void Update()
		{
			if(Scoreboard)
				Scoreboard.text = string.Format(format, Score);
		}

		public void AddScore(int toAdd)
		{
			Score += toAdd;
		}

		public void RemoveScore(int toRemove)
		{
			Score -= toRemove;
			Score = Mathf.Max(0, Score);
		}

		public void ResetScore()
		{
			Score = 0;
		}
	}
}