﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GMTK
{
	[RequireComponent(typeof(Enemy))]
	[RequireComponent(typeof(Rigidbody2D))]
	public class EnemyPatrolAI : MonoBehaviour
	{
		public Enemy enemy;
		public Rigidbody2D rb;

		public float speed = 10;
		public List<Transform> route;

		private int nextTarget = 0;

		void Start()
		{
			if (enemy == null)
			{
				enemy = GetComponent<Enemy>();
			}
			if (rb == null)
			{
				rb = GetComponent<Rigidbody2D>();
			}
		}

		void Update()
		{
			if (route.Count == 0) return;

			if (Vector2.Distance(route[nextTarget].position, rb.position) < 1)
			{
				nextTarget = (nextTarget + 1) % route.Count;
			}

			Vector2 targetingVector = route[nextTarget].position - gameObject.transform.position;
			targetingVector.Normalize();
			rb.velocity = targetingVector * speed;
		}

	}
}