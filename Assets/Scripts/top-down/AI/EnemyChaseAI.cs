﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GMTK
{
	[RequireComponent(typeof(Enemy))]
	[RequireComponent(typeof(Rigidbody2D))]
	public class EnemyChaseAI : MonoBehaviour
	{
		public Enemy enemy;
		public Rigidbody2D rb;

		public float speed = 10;
		public float sightRange = 5;

		void Start()
		{
			if (enemy == null)
			{
				enemy = GetComponent<Enemy>();
			}
			if (rb == null)
			{
				rb = GetComponent<Rigidbody2D>();
			}
		}

		void Update()
		{
			Vector2 targetingVector = enemy.target.position - gameObject.transform.position;
			if (targetingVector.magnitude <= sightRange)
			{
				targetingVector.Normalize();
				rb.velocity = targetingVector * speed;
			}
		}

	}
}