﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GMTK
{
	[RequireComponent(typeof(Shooter))]
	[RequireComponent(typeof(Enemy))]
	public class EnemyShootAI : MonoBehaviour
	{
		public Shooter shooter;
		public Enemy enemy;

		public float turnsPerSecond = 1;
		public float shotsPerSecond = 3;
		public float sightRange = 5;

		private Vector3 angle = new Vector3(1, 0, 0);

		void Start()
		{
			if (shooter == null)
			{
				shooter = GetComponent<Shooter>();
			}
			shooter.ROF = 1 / shotsPerSecond;

			if (enemy == null)
			{
				enemy = GetComponent<Enemy>();
			}
		}

		void Update()
		{
			if (enemy.target == null)
				return;

			Vector3 targetingVector = enemy.target.position - gameObject.transform.position;
			if (targetingVector.magnitude <= sightRange)
			{
				angle = Vector3.RotateTowards(angle, targetingVector, 2 * Mathf.PI * turnsPerSecond * Time.deltaTime, 0);
				shooter.Shoot(angle, 0);
			}
		}

	}
}