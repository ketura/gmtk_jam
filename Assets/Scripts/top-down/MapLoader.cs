﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace GMTK
{
	[Serializable]
	public struct ColorKeyedObject
	{
		public Color color;
		public GameObject prefab;
	}

	public class MapLoader : Singleton<MapLoader>
	{
		public Transform MapParent;

		public Texture2D DefaultMap;
		public float zOffset = 0;
		public Vector2 tileOffset = new Vector2(1, 1);
		public List<ColorKeyedObject> colorToObject;

		void Start()
		{
			if (DefaultMap != null)
				LoadMap(DefaultMap);
		}

		public void LoadMap(Texture2D map)
		{
			if (map == null || colorToObject == null)
			{
				throw new System.Exception("Map Loader not given valid data");
			}
			Dictionary<Color, GameObject> cto = new Dictionary<Color, GameObject>();
			foreach (ColorKeyedObject cko in colorToObject)
			{
				cto.Add(cko.color, cko.prefab);
				Debug.Log(string.Format("Found key of color: {0}", cko.color));
			}
			if (colorToObject.Count != cto.Count)
			{
				throw new System.Exception("Repeated color in MapLoader");
			}

			Debug.Log(string.Format("{0} pixels to process in map", map.width * map.height));
			Color[] pixels = map.GetPixels(0, 0, map.width, map.height);
			Vector3 tlOffset = new Vector3(-tileOffset.x * map.width / 2, -tileOffset.y * map.height / 2, zOffset);
			for (var i = 0; i < pixels.Length; i++)
			{
				//Debug.Log(pixels[i]);
				//if (cto.ContainsKey(pixels[i]))
				//	Debug.Log("Found:\n", cto[pixels[i]]);
				if (cto.ContainsKey(pixels[i]) && cto[pixels[i]] != null)
				{
					
					//Debug.Log(string.Format("Placing block at pixel {0}, {1} of colour {2}", i % map.width, Math.Floor((double)i / map.width), pixels[i]));
					Instantiate(cto[pixels[i]], tlOffset +

							new Vector3(tileOffset.x * (i % map.width),
										tileOffset.y * (float)Math.Floor((double)i / map.width),
													0),
							 Quaternion.identity, MapParent);
				}
				else
				{
					//Debug.Log(string.Format("Color at {0}, {1} not matched ({2})",
					//												i % map.width,
					//												Math.Floor((double)i / map.width),
					//											 pixels[i]));
				}
			}

			GameHandler.Instance.Load();
		}
	}
}
