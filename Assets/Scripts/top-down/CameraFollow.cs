﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace GMTK
{
	[RequireComponent(typeof(Camera))]
	public class CameraFollow : MonoBehaviour 
	{
		public float CameraDistance = 6;
		public float dampTime = 0.15f;
		private Vector3 velocity = Vector3.zero;
		public Player player;
		public Transform target;
		public float MaxSpeed=10;
		public float maxspeed;

		public Camera Camera;

		private void Start()
		{
			Camera = GetComponent<Camera>();
			target = player.transform;

			maxspeed = MaxSpeed;
		}

		// Update is called once per frame
		void FixedUpdate()
		{
			if (target)
			{
				Vector3 point = Camera.WorldToViewportPoint(target.position);
				Vector3 delta = target.position - Camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
				Vector3 destination = transform.position + delta;

				float damp = dampTime;
				if (Mathf.Abs(delta.x) > 5 || Mathf.Abs(delta.y) > 5)
				{
					maxspeed = Mathf.Max(player.CurrentSpeed, Mathf.Abs(Vector3.Distance(target.position, transform.position)) * 30);
					if (maxspeed > MaxSpeed)
						damp -= damp / maxspeed * 2;
					//damp = 1/ player.CurrentSpeed;
				}
				if (Mathf.Abs(delta.x) > 9 || Mathf.Abs(delta.y) > 9)
				{
					maxspeed = Mathf.Max(player.CurrentSpeed * 1.5f, MaxSpeed *10 * Mathf.Max(Mathf.Abs(delta.x), Mathf.Abs(delta.x)));
					damp = 0.001f;

				}
				else if (Mathf.Abs(delta.x) < 1 && Mathf.Abs(delta.y) < 1)
				{
					maxspeed = MaxSpeed;
				}
				transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, damp, Mathf.Max(player.CurrentSpeed *0.9f, maxspeed));

				//Vector3 moveCamera = new Vector3(player.transform.position.x, player.transform.position.x, 0.6f);
				//transform.position = Vector3.Lerp(transform.position, moveCamera, dampTime * Time.deltaTime);
			}

		}
	}
}
