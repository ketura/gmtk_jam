﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GMTK
{
	public class Damageable : MonoBehaviour
	{

		public int MaxHP = 1;
		public int HP = 1;

		void Start()
		{
			HP = MaxHP;
		}

		public bool IsAlive { get { return HP > 0; } }

		public void Damage(int damage)
		{
			HP -= damage;
		}

		public void AddHealth()
		{
			HP = Mathf.Min(HP + 1, MaxHP);
		}
	}
}