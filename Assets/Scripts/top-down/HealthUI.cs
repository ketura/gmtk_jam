﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace GMTK
{
	public class HealthUI : MonoBehaviour 
	{
		public GameObject TogglePrefab;
		public HorizontalLayoutGroup panel;

		public List<Toggle> bits;

		private int currentHealth;

		public void UpdateMaxHealth(int count)
		{
			foreach(var bit in bits)
			{
				Destroy(bit.gameObject);
				bits.Remove(bit);
			}

			for (int i = 0; i < count; i++)
			{
				var go = Instantiate(TogglePrefab, panel.transform, false);
				bits.Add(go.GetComponent<Toggle>());
			}

			UpdateCurrentHealth(currentHealth);
		}

		public void UpdateCurrentHealth(int amount)
		{
			currentHealth = amount;

			foreach(var toggle in bits)
			{
				toggle.graphic.gameObject.SetActive(false);
			}

			for (int i = 0; i < Mathf.Min(amount, bits.Count); i++)
			{
				bits[i].graphic.gameObject.SetActive(true);
			}
		}
	}
}
