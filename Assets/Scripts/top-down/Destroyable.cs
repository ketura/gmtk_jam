﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace GMTK
{
	[RequireComponent(typeof(UnitColor))]
	public class Destroyable : MonoBehaviour 
	{

		public UnitColor color;
		void Start () 
		{
			color = GetComponent<UnitColor>();
		}

		void OnTriggerEnter2D(Collider2D coll)
		{
			if (coll.gameObject.tag != "Destroyer")
				return;

			var player = coll.gameObject.GetComponent<BlockDestroyer>().player;

			if (!player.Charge.SuperMode)
				return;

			//Debug.Log("speed: " + player.CurrentSpeed + ", player: " + player.Color.Color + ", this: " + color.Color);

			if (color.Color == player.Color.Color && player.CurrentSpeed > color.DestructionSpeed)
				color.Destroy();

		}
	}
}
