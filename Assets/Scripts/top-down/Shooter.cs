﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace GMTK
{
	public class Shooter : MonoBehaviour 
	{
		public GameObject BulletPrefab;

		public float ROF = 1;
		public float BulletSpeed = 5;
		public int BulletDamage = 1;
		public float ScoreFactor = 2;

		public float SpeedBonus;
		public float SupermodeBonus;

		public TargetType Targets;

		private float LastShotTime;
		private float trueROF;

		private ScoreManager Scoreboard;

		private void Start()
		{
			LastShotTime = Time.time;
			trueROF = ROF;

			Scoreboard = GameObject.FindGameObjectWithTag("Scoreboard").GetComponent<ScoreManager>();
		}

		public virtual Bullet[] Shoot(Vector3 angle, float baseSpeed, bool supermode = false)
		{
			baseSpeed = Mathf.Max(baseSpeed, 10f);
			trueROF = ROF / (baseSpeed / SpeedBonus);
			if (Scoreboard.Score == 0)
				trueROF = ROF * ScoreFactor;
			if (supermode)
				trueROF *= SupermodeBonus;

			if (Time.time - LastShotTime < trueROF)
				return new Bullet[0];

			LastShotTime = Time.time;
			GameObject go = Instantiate(BulletPrefab, transform.position, Quaternion.identity, null);
			Bullet bullet = go.GetComponent<Bullet>();
			bullet.Speed = BulletSpeed + baseSpeed;
			bullet.Damage = BulletDamage;
			bullet.Heading = angle;
			bullet.Targets = Targets;

			return new Bullet[] { bullet };

		}
	}
}
