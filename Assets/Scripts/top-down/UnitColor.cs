﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace GMTK
{

	public enum EntityColor { Black, White, Grey, Neutral }

	public class UnitColor : MonoBehaviour 
	{
		public EntityColor Color = EntityColor.Black;

		public float DestructionSpeed;

		public void Start()
		{
			UpdateLayer();
		}

		public void UpdateLayer()
		{
			switch (Color)
			{
				case EntityColor.Black:
					gameObject.layer = LayerMask.NameToLayer("Black");
					break;
				case EntityColor.White:
					gameObject.layer = LayerMask.NameToLayer("White");
					break;
				case EntityColor.Grey:
					gameObject.layer = LayerMask.NameToLayer("Grey");
					break;
				case EntityColor.Neutral:
					gameObject.layer = LayerMask.NameToLayer("Neutral");
					break;
			}

		}

		public void Destroy()
		{
			GameObject.Destroy(this.gameObject);
		}
	}
}
