﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace GMTK
{
	public class Wall : MonoBehaviour 
	{
		private void OnTriggerEnter2D(Collider2D other)
		{
			if (other.gameObject.tag == "Bullet")
			{
				var bullet = other.gameObject.GetComponent<Bullet>();
				bullet.BulletImpact();
			}


		}
	}
}
