﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using Utilities;

namespace GMTK
{
	
	[RequireComponent(typeof(UnitColor))]
	[RequireComponent(typeof(ChargeUp))]
	[RequireComponent(typeof(Damageable))]
	public class Player : MonoBehaviour 
	{
		public float Acceleration = 1;
		public float Deceleration = 1;
		public float DashAcceleration = 5;
		public float SuperDashAcceleration = 5;
		public float BaseSpeed = 3;
		public float SuperSpeedFactor = 1.5f;
		public float ChargeAcceleration { get { return Acceleration + (Charge.SuperPercentage * Acceleration) * SuperSpeedFactor; } }
		public float BaseMaxSpeed = 100;
		public float BonusMaxSpeed;
		public float SpeedLostOnHit = 5;
		public float MaxSpeed { get { return BaseMaxSpeed + BonusMaxSpeed; } }
		public float SquashFactor = 10.0f;

		public float ExplosionDuration = 0.25f;
		public int ExplosionDamage = 1;
		public int SuperExplosionDamage = 2;

		public int DashDamage = 1;
		public int SuperDashDamage = 1;

		public float DashDuration = 1;
		public float SuperDashDuration = 0.5f;
		public int DashCost = 100;
		public float ShieldDuration = 1;
		public int ShieldHealth = 2;
		public float SuperShieldDuration = 2f;
		public int SuperShieldHealth = 3;
		public int ShieldCost = 100;
		public int BulletCost = 200;

		public float EnemyDestroyedBarPercent = 0.10f;

		public float SpeedDamageFactor = 0.25f;
		public float SpeedShieldThreshold = 25;

		public float SuperModeDiscount = 0.5f;

		public Vector2 CurrentDirection;
		public float CurrentSpeed;

		public Rigidbody2D RigidBody;

		public Transform Body;
		public GameObject Sword;
		public GameObject Shield;

		public KeyCode LastPressed;
		public float DashFactor;

		public Material BlackMat;
		public Material WhiteMat;
		public UnitColor Color;
		public MeshRenderer Fuselage;

		public Explosion explosion;
		public DashCone sword;

		public Shooter shooter;
		public Damageable Health;

		private int BlackLayer;
		private int WhiteLayer;
		private bool ShieldUp;

		private ScoreManager Scoreboard;

		public ChargeUp Charge;
		public bool SuperMode { get { return Charge.SuperMode; } }

		private int WallCount;
		public bool WallContact { get { return WallCount > 0; } }
		public float MaxWallSpeed;

		public GameObject ShipModel;

		public float InvulnerabilityTime = 2;
		public float FlashFrequency = 0.5f;

		public HealthUI healthUI;

		void Start () 
		{
			CurrentDirection = Vector2.zero;

			if (RigidBody == null)
				RigidBody = this.GetComponent<Rigidbody2D>();

			LastPressed = KeyCode.None;
			DashFactor = 0;

			Shield.SetActive(false);
			shooter = this.GetComponent<Shooter>();

			Color = GetComponent<UnitColor>();
			Charge = GetComponent<ChargeUp>();
			SetColor(EntityColor.Black);

			Health = GetComponent<Damageable>(); 

			BlackLayer = LayerMask.NameToLayer("Black");
			WhiteLayer = LayerMask.NameToLayer("White");

			Scoreboard = GameObject.FindGameObjectWithTag("Scoreboard").GetComponent<ScoreManager>();
			Scoreboard.AddScore(1000);

			explosion = GameObject.FindGameObjectWithTag("Explosion").GetComponent<Explosion>();
			explosion.gameObject.SetActive(false);

			sword = GameObject.FindGameObjectWithTag("DashCone").GetComponent<DashCone>();
			sword.gameObject.SetActive(false);

			healthUI = GetComponent<HealthUI>();
			healthUI.UpdateMaxHealth(Health.MaxHP);
			healthUI.UpdateCurrentHealth(Health.HP);
		}

		public Vector2 DirectionFromKey(KeyCode key)
		{
			switch (key)
			{
				case KeyCode.W:
					return Vector2.up;
				case KeyCode.S:
					return Vector2.down;
				case KeyCode.A:
					return Vector2.left;
				case KeyCode.D:
					return Vector2.right;
			}

			return Vector2.zero;
		}

		private void SetColor(EntityColor color)
		{
			switch (color)
			{
				case EntityColor.Black:
					Fuselage.material = BlackMat;
					break;
				case EntityColor.White:
					Fuselage.material = WhiteMat;
					break;
			}

			Color.Color = color;
			Color.UpdateLayer();
			Charge.UpdateColor(color);
		}

		void Update () 
		{
			if (!Health.IsAlive)
			{
				GameHandler.Instance.GameOver(Scoreboard.Score);
			}

			//Figure out which directional keys are being used
			List<KeyCode> KeysHeld = new List<KeyCode>();

			if (Input.GetButton("Up"))
				KeysHeld.Add(KeyCode.W);
			if (Input.GetButton("Down"))
				KeysHeld.Add(KeyCode.S);
			if (Input.GetButton("Left"))
				KeysHeld.Add(KeyCode.A);
			if (Input.GetButton("Right"))
				KeysHeld.Add(KeyCode.D);

			if (Input.GetButtonDown("Up"))
				LastPressed = KeyCode.W;
			else if (Input.GetButtonDown("Down"))
				LastPressed = KeyCode.S;
			else if (Input.GetButtonDown("Left"))
				LastPressed = KeyCode.A;
			else if (Input.GetButtonDown("Right"))
				LastPressed = KeyCode.D;

			//Determine speed based on keys pressed
			if (KeysHeld.Count > 0)
			{
				
				if (CurrentSpeed == 0)
					CurrentSpeed = BaseSpeed;

				if(CurrentSpeed < BaseMaxSpeed)
					CurrentSpeed += ChargeAcceleration * Time.deltaTime;

				if (WallContact && CurrentSpeed > MaxWallSpeed)
				{
					CurrentSpeed = MaxWallSpeed;
				}
			}

			switch (KeysHeld.Count)
			{
				case 0:
				default:
					LastPressed = KeyCode.None;
					if (CurrentSpeed > 0)
						CurrentSpeed = Mathf.Max(0, CurrentSpeed - Deceleration);
					//Body.localScale = Vector3.one;
					break;

				case 1:
					CurrentDirection = DirectionFromKey(KeysHeld[0]);
					break;

				case 2:
				case 3:
				case 4:
					if (KeysHeld.Contains(LastPressed))
						CurrentDirection = DirectionFromKey(LastPressed);
					else
						CurrentDirection = DirectionFromKey(KeysHeld[0]);
					break;

			}

			//Get model direction to match speed

			float direction = 0;
			if (CurrentDirection.x != 0)
				direction = -90 * CurrentDirection.x;
			else if (CurrentDirection.y == -1)
				direction = 180;

			Body.transform.localRotation = Quaternion.Euler(0, 0, direction);


			//Activate dash/black 
			if (Input.GetButtonDown("BlackDash"))
				StartCoroutine(Dash());

			//activate shield/white
			if(Input.GetButtonDown("WhiteShield"))
				StartCoroutine(ShieldActivate());

			//shoot
			if (Input.GetButton("Shoot"))
			{
				int count = shooter.Shoot(CurrentDirection, CurrentSpeed).Length;
				int cost = (int)(BulletCost * count * (Charge.SuperMode ? SuperModeDiscount : 1.0f));
				Scoreboard.RemoveScore(cost);
			}
				

			//modify speed based on dashing, cap speed
			CurrentSpeed += DashFactor;
			CurrentSpeed = Mathf.Clamp(CurrentSpeed, 0, CurrentSpeed);

			//http://puu.sh/wKjap/35c0cd21e2.png
			//Mathf.Sqrt(Mathf.Pow((2 * horizontal * damping), 2) + );


			//Apply speed to the physics objects
			RigidBody.velocity = CurrentDirection * CurrentSpeed;

			Charge.UpdateSpeed(CurrentSpeed);

			//squash the model based on speed
			if (CurrentDirection.x != 0)
				transform.localScale = new Vector3(Mathf.Max(CurrentSpeed / SquashFactor, 1.0f), Mathf.Min(SquashFactor / CurrentSpeed, 1.0f), Body.localScale.z);
			if (CurrentDirection.y != 0)
				transform.localScale = new Vector3(Mathf.Min(SquashFactor / CurrentSpeed, 1.0f), Mathf.Max(CurrentSpeed / SquashFactor, 1.0f), Body.localScale.z);


		}

		void OnCollisionEnter2D(Collision2D coll)
		{
			if (coll.gameObject.tag == "Wall")
			{
				var othercolor = coll.gameObject.GetComponent<UnitColor>();
				if (othercolor == null || othercolor.Color != Color.Color)
					CurrentSpeed = 0;
				WallCount++;
			}
			
		}

		

		private void OnCollisionExit2D(Collision2D coll)
		{
			if (coll.gameObject.tag == "Wall")
			{
				WallCount--;
			}
		}

		private void OnTriggerEnter2D(Collider2D other)
		{
			if (other.gameObject.tag == "Points")
			{
				Debug.Log("got points");
				var points = other.gameObject.GetComponent<PointPickup>();
				Scoreboard.AddScore(points.Amount);
				points.Pickup();
			}
			else if(other.gameObject.tag == "Bullet")
			{
				var bullet = other.gameObject.GetComponent<Bullet>();
				if(bullet.Targets == TargetType.Player && !invuln)
				{
					TakeHit(bullet.Damage);
					bullet.BulletImpact();
				}
			}
			else if (other.gameObject.tag == "Health")
			{
				Debug.Log("got health");
				var health = other.gameObject.GetComponent<HealthPickup>();
				Health.AddHealth();
				health.Pickup();
				healthUI.UpdateCurrentHealth(Health.HP);
			}
			else if (other.gameObject.tag == "Enemy")// && !invuln)
			{
				Debug.Log("touched enemy");
				var enemy = other.gameObject.GetComponent<Enemy>();
				if (ShieldUp && CurrentSpeed > SpeedShieldThreshold)
				{
					enemy.TakeHit((int)(CurrentSpeed / SpeedShieldThreshold));
				}

				TakeHit(enemy.touchDamage);
			}

		}

		public void TakeHit(int damage)
		{
			if (invuln)
				return;

			if (ShieldUp)
			{
				ShieldHit(damage);
				return;
			}

			Health.Damage(damage);
			healthUI.UpdateCurrentHealth(Health.HP);
			CurrentSpeed -= SpeedLostOnHit;
			BonusMaxSpeed = 0;
			StartCoroutine(Invulnerable());
		}

		public void ShieldHit(int damage)
		{
			if (!SuperMode)
			{
				//ShieldUp = false;
				//Shield.SetActive(false);
			}
		}

		public void EnemyDestroyed(float speedBoost, float maxSpeedBoost)
		{
			Debug.Log("enemy destroyed");
			Debug.Log(BonusMaxSpeed);
			BonusMaxSpeed += maxSpeedBoost;
			Debug.Log(BonusMaxSpeed);
			CurrentSpeed += speedBoost;
			Charge.ChargeAmount += Charge.ChargeTime * EnemyDestroyedBarPercent;
		}

		private bool dashing = false;
		public IEnumerator Dash(float duration = 0)
		{
			

			if(!dashing)
			{
				SetColor(EntityColor.Black);

				if (Scoreboard.Score == 0)
					yield break;

				if (ShieldUp)
				{
					StartCoroutine(ShieldAttack());
					yield break;
				}

				dashing = true;
				int cost = (int)(DashCost * (Charge.SuperMode ? SuperModeDiscount : 1.0f));
				Scoreboard.RemoveScore(cost);

				sword.Damage = SuperMode ? SuperDashDamage : DashDamage;

				Sword.SetActive(true);

				if (duration == 0)
					duration = Charge.SuperMode ? SuperDashDuration : DashDuration;

				float accel = Charge.SuperMode ? SuperDashAcceleration : DashAcceleration;

				DashFactor = accel / 2;
				yield return new WaitForSeconds(duration / 3);

				if (!dashing)
					DashFactor = 0;
				else
					DashFactor = accel;

				yield return new WaitForSeconds(duration / 6);

				if (!dashing)
					DashFactor = 0;
				else
					DashFactor = -DashAcceleration * 0.9f;

				yield return new WaitForSeconds(duration / 3);

				
				Sword.SetActive(false);

				DashFactor = 0;
				if (CurrentSpeed > MaxSpeed)
					CurrentSpeed = MaxSpeed;

				dashing = false;
			}
		}

		public IEnumerator ShieldAttack()
		{
			explosion.gameObject.SetActive(true);
			Shield.SetActive(false);
			Sword.SetActive(false);
			ShieldUp = false;

			int cost = (int)(DashCost + ShieldCost * (Charge.SuperMode ? SuperModeDiscount : 1.0f));
			Scoreboard.RemoveScore(cost);

			explosion.Damage = SuperMode ? SuperExplosionDamage : ExplosionDamage;

			yield return new WaitForSeconds(ExplosionDuration);

			explosion.gameObject.SetActive(false);
		}

		private bool shielding = false;

		public IEnumerator ShieldActivate(float duration=0)
		{
			if (!shielding)
			{
				SetColor(EntityColor.White);

				if (Scoreboard.Score == 0)
					yield break;


				if (dashing)
				{
					StartCoroutine(ShieldAttack());
					yield break;
				}

				shielding = true;
				ShieldUp = true;
				int cost = (int)( ShieldCost * (Charge.SuperMode ? SuperModeDiscount : 1.0f));
				Scoreboard.RemoveScore(cost);
				DashFactor = 0;

				Shield.SetActive(true);

				if (duration == 0)
					duration = Charge.SuperMode ? SuperShieldDuration : ShieldDuration;

				yield return new WaitForSeconds(duration);

				Shield.SetActive(false);

				
				ShieldUp = false;

				shielding = false;
			}
		}

		private bool invuln = false;
		IEnumerator Invulnerable()
		{
			if(!invuln)
			{
				invuln = true;

				float timer = 0;
				float flashtimer = 0;

				while(timer < InvulnerabilityTime)
				{
					timer += Time.deltaTime;
					flashtimer += Time.deltaTime;

					if(flashtimer > FlashFrequency)
					{
						ShipModel.SetActive(!ShipModel.activeSelf);
						flashtimer = 0;
					}


					yield return new WaitForEndOfFrame();
				}

				ShipModel.SetActive(true);

				invuln = false;
			}
		}



		public void Interrupt()
		{
			CurrentSpeed = 0;
		}

	}
}
