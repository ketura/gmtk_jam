﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace GMTK
{
	public enum TargetType { Player, Enemy }

	[RequireComponent(typeof(Rigidbody2D))]
	public class Bullet : MonoBehaviour 
	{
		public float Speed;
		public int Damage;

		public TargetType Targets;

		public Rigidbody2D RB;
		public Vector3 Heading;

		public float DespawnDistance = 100;

		private Vector3 originalPos;

		void Start()
		{
			RB = this.GetComponent<Rigidbody2D>();

			originalPos = this.transform.position;
		}

		void Update()
		{
			RB.velocity = Heading * Speed;

			// Sanity culling
			if (Vector3.Distance(transform.position, originalPos) > DespawnDistance)
				Destroy(gameObject);
		}

		public void BulletImpact()
		{
			Destroy(gameObject);
		}

		//private void OnCollisionEnter2D(Collision2D coll)
		//{
		//	if (targetTags.Contains(coll.gameObject.tag))
		//	{
		//		Damageable target = coll.gameObject.GetComponent<Damageable>();
		//		if (target != null)
		//		{
		//			target.Damage(Damage);
		//		}
		//		Destroy(gameObject);
		//	}
		//}
	}
}
