﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace GMTK
{
	public class ChargeUp : MonoBehaviour 
	{

		public Image ChargeForeground;
		public Slider ChargeBar;

		public float ChargeTime;
		public float ChargeThreshold;
		public float BleedFactor;
		public float GraceAmount;

		public float MaxLossFactor;

		private bool qualified;

		public float ChargeAmount;

		private float GraceTimer;

		private float LastSpeed;

		private Player player;

		public float SuperDuration;
		public float SuperTimeRemaining;
		public float SuperPercentage { get { return SuperTimeRemaining / SuperDuration; } }
		public bool SuperMode;

		void Start () 
		{
			player = GetComponent<Player>();
		}

		void Update () 
		{
			if (ChargeAmount >= ChargeTime)
			{
				SuperMode = true;
				SuperTimeRemaining = SuperDuration;
				ChargeAmount = 0;
			}

			if (SuperMode)
			{
				ChargeForeground.color = Color.red;
				SuperTimeRemaining -= Time.deltaTime;

				ChargeBar.value = SuperPercentage * 100;
				if (SuperTimeRemaining <= 0)
					SuperMode = false;
				else
					return;
			}


			if (qualified)
			{
				ChargeAmount = Mathf.Clamp(ChargeAmount + Time.deltaTime, 0, ChargeTime);
				GraceTimer = 0;
			}
			else
			{
				GraceTimer += Time.deltaTime;
				if (GraceTimer > GraceAmount)
					ChargeAmount = Mathf.Max(ChargeAmount - Time.deltaTime * BleedFactor, 0);
			}

			ChargeBar.value = ChargeAmount / ChargeTime * 100;

		}

		public void UpdateSpeed(float speed)
		{
			if (!SuperMode)
			{
				qualified = speed >= ChargeThreshold;
				if (speed == 0 && LastSpeed != 0)
					ChargeAmount -= ((LastSpeed / 100) * ChargeAmount * MaxLossFactor) / ChargeAmount;
				LastSpeed = speed;
			}
		}

		public void UpdateColor(EntityColor color)
		{
			switch (color)
			{
				case EntityColor.Black:
					ChargeForeground.color = Color.black;
					break;
				case EntityColor.White:
					ChargeForeground.color = Color.white;
					break;
			}
		}
	}
}
