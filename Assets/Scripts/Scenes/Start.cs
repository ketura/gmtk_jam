﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

using UnityEngine.SceneManagement;

namespace GMTK
{
	public class Start : MonoBehaviour 
	{
		public void OnStart()
		{
			SceneManager.LoadScene("movement-test");
		}

		public void OnQuit()
		{
			ExitHandler.Instance.GracefulExit();
		}
	}
}
