﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace GMTK
{
	public class GameOver : MonoBehaviour 
	{
		public GameHandler handler;
		public TMPro.TMP_Text MessageText;
		public TMPro.TMP_Text scoreText;
		void Start () 
		{
			handler = GameHandler.Instance;
			MessageText.text = handler.LostGame ? "GAME OVER" : "YOU WIN!";
			scoreText.text = "Final score: " + handler.Points;
		}

		void Update () 
		{
			
		}
	}
}
