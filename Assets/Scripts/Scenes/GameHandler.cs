﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

using UnityEngine.SceneManagement;
using TMPro;

namespace GMTK
{
	public class GameHandler : Singleton<GameHandler>
	{
		public Player player;
		public Camera MainCamera;

		public Texture2D MapToLoad;

		public Transform spawn;

		public int Points;
		public bool LostGame;

		private void Awake()
		{
			SceneManager.sceneLoaded += OnLevelLoad;
		}

		public void OnLevelLoad(Scene scene, LoadSceneMode mode)
		{
			if (scene.name == "movement-test")
			{
				MapLoader.Instance.LoadMap(MapToLoad);
				ScoreManager.Instance.Scoreboard = GameObject.FindGameObjectWithTag("ScoreText").GetComponent<TMP_Text>();
				DontDestroyOnLoad(transform.parent.gameObject);
			}
		}

		public void Load()
		{
			player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
			Debug.Log(player.transform.position);
			MainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
			spawn = GameObject.FindGameObjectWithTag("PlayerSpawn").transform;


			Debug.Log("Relocating player + camera");

			player.transform.position = spawn.position;
			Debug.Log(player.transform.position);
			Debug.Log(spawn.transform.position);
			MainCamera.transform.position = spawn.position + new Vector3(0, 0, -10);
		}

		public void GameOver(int finalscore)
		{
			Points = finalscore;
			LostGame = true;
			SceneManager.LoadScene("GameOver");
		}

		public void YouWin(int finalscore)
		{
			Points = finalscore;
			LostGame = false;
			SceneManager.LoadScene("GameOver");
		}
		

		void Update () 
		{
			if (Input.GetKey(KeyCode.Escape))
				ExitHandler.Instance.GracefulExit();
		}
	}
}
