﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace GMTK
{
	public class CreditsExit : MonoBehaviour 
	{
		public float TimeToWait = 3.0f;
		public float timer;

		public float TimeToQuit = 10.0f;
		void Start () 
		{
		
		}

		void Update () 
		{
			if (Input.GetKey(KeyCode.Escape))
				ExitHandler.Instance.InstantExit();

			timer += Time.deltaTime;

			if (timer > TimeToWait)
			{
				if (Input.anyKey || timer > TimeToQuit)
					ExitHandler.Instance.InstantExit();
			}

		}
	}
}
