﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace GMTK
{
	public class EndTRigger : MonoBehaviour 
	{
		void OnCollisionEnter2D(Collision2D coll)
		{
			if (coll.gameObject.tag == "Player")
			{
				GameHandler.Instance.YouWin(ScoreManager.Instance.Score);
			}

		}
	}
}
